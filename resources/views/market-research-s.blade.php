<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from consultar.webps.info/market-research-s.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Jul 2022 16:34:22 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="" />
	<meta name="description" content="Market Research">
	<meta name="author" content="Admin">    
    <link rel="shortcut icon" type="image/png" href="assets/themes/consultar/assets/images/favicon.png">
    <title>Market Research | Consultar - Consulting Business MODX Theme</title>
    <link href="{{ url('assets/themes/consultar/assets/css/themify-icons.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/flaticon.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/animate.css')}}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/owl.theme.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/slick.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/slick-theme.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/swiper.min.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/owl.transitions.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/jquery.fancybox.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/odometer-theme-default.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/component.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/responsive.css') }}" rel="stylesheet">
</head>


<body>
    <!-- start page-wrapper -->
    <div class="page-wrapper">
        <!-- start preloader -->
        <div class="preloader">
            <div class="angular-shape">
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div class="spinner">
              <div class="double-bounce1"></div>
              <div class="double-bounce2"></div>
            </div>
        </div>
        <!-- end preloader -->
        <!-- Start header -->
        <header id="header">
            <div class="topbar">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col col-md-7 col-sm-12 col-12">
                            <div class="contact-intro">
                                <ul>
                                    <li><i class="fi ti-location-pin"></i>7 Green Lake Street Crawfordsville</li>
                                    <li><i class="fi flaticon-email"></i> consultar@mail.com</li>
                                </ul>
                            </div>
                        </div>                        
                        <div class="col col-md-5 col-sm-12 col-12">
                            <div class="contact-info">
                                <ul>
                                    <li>Visit our social pages</li>
                                    <li><a target="_blank" href="https://facebook.com/"><i class="ti-facebook"></i></a></li>
                                    <li><a target="_blank" href="https://twitter.com/"><i class="ti-twitter-alt"></i></a></li>
                                    <li><a target="_blank" href="https://www.instagram.com/"><i class="ti-instagram"></i></a></li>
                                    <li><a target="_blank" href="https://www.google.com/"><i class="ti-google"></i></a></li>
                                </ul>
                            </div>
                        </div>    
                    </div>
                </div>
            </div> <!-- end topbar -->
            <div class="wpo-site-header">
                <nav class="navigation navbar navbar-expand-lg navbar-light">
                    <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-lg-3 col-md-3 col-6">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggler open-btn">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar first-angle"></span>
                                    <span class="icon-bar middle-angle"></span>
                                    <span class="icon-bar last-angle"></span>
                                </button>
                                <a class="navbar-brand" href="index.html"><img src="assets/themes/consultar/assets/images/logo.png"></a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-2 col-1">
                            <div id="navbar" class="collapse navbar-collapse navigation-holder">
                                <button class="close-navbar"><i class="ti-close"></i></button>
                                <ul class="nav navbar-nav mb-2 mb-lg-0">
                                    <li>
        								<a href="index.html">
        									Home
        								</a>
        							</li>
                                    <li>
        								<a href="about.html">
        									About
        								</a>
        							</li>
                                    <li>
        								<a href="services.html">
        									Services
        								</a>
        							</li>
                                    <li>
        								<a href="projects.html">
        									Projects
        								</a>
        							</li>
                                        <li class="menu-item-has-children">
    									<a href="#">
    											Pages
    										</a>
    										<ul class="sub-menu">
                                    <li>
        								<a href="pricing.html">
        									Pricing
        								</a>
        							</li>
                                    <li>
        								<a href="testimonial.html">
        									Testimonial
        								</a>
        							</li>

                                            </ul>
    									</li>
											
                                    <li>
        								<a href="blog.html">
        									Blog
        								</a>
        							</li>
                                    <li>
        								<a href="contacts.html">
        									Contacts
        								</a>
        							</li>

                                </ul>
                                <div id="dl-menu" class="dl-menuwrapper">
                                    <button class="dl-trigger">Open Menu</button>
                                    <ul class="dl-menu">
                                    <li>
        								<a href="index.html">
        									Home
        								</a>
        							</li>
                                    <li>
        								<a href="about.html">
        									About
        								</a>
        							</li>
                                    <li>
        								<a href="services.html">
        									Services
        								</a>
        							</li>
                                    <li>
        								<a href="projects.html">
        									Projects
        								</a>
        							</li>
                                        <li>
    									<a href="#">
    											Pages
    										</a>
    										<ul class="dl-submenu">
                                    <li>
        								<a href="pricing.html">
        									Pricing
        								</a>
        							</li>
                                    <li>
        								<a href="testimonial.html">
        									Testimonial
        								</a>
        							</li>

                                            </ul>
    									</li>
											
                                    <li>
        								<a href="blog.html">
        									Blog
        								</a>
        							</li>
                                    <li>
        								<a href="contacts.html">
        									Contacts
        								</a>
        							</li>

                                    </ul>
                                </div><!-- /dl-menuwrapper -->
                            </div><!-- end of nav-collapse -->
                        </div>
                        <div class="col-lg-3 col-md-4 col-4">
                            <div class="header-right">
                                <div class="close-form">
                                    <a target="_blank" class="theme-btn" href="contacts.html">Free Consulting</a>
                                </div>
                                <div class="header-search-form-wrapper">
                                    <div class="cart-search-contact">
                                        <button class="search-toggle-btn"><i class="fi flaticon-magnifiying-glass"></i></button>
                                        <div class="header-search-form">
                                            <form role="search" action="https://consultar.webps.info/search.html" method="get" id="searchform"  class="form">
                                                <div>
                                                    <input type="text" class="form-control" name="search" placeholder="Search here...">
                                                    <button  type="submit" class="btn"><i class="fi flaticon-magnifiying-glass"></i></button>
                                                    <input type="hidden" name="id" value="16" />
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end of container -->
                </nav>
            </div>
        </header>
        <!-- end of header -->
        <!-- start of breadcumb-section -->
        <div class="wpo-breadcumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-breadcumb-wrap">
                            <h2>Market Research</h2>
                            <ul id="breadcrumb" itemprop="breadcrumb"><li><a href="index.html">Home</a></li>
                                <li><a title="" href="services.html">Services</a></li>
                                <li><span>Market Research</span></li></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of wpo-breadcumb-section-->
        <!-- start of wpo-service-single-section -->
        <section class="wpo-service-single-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-12">
                        <div class="wpo-service-single-wrap">
                            <div class="wpo-service-single-content">
                                <img src="assets/themes/consultar/assets/images/service-single/1.jpg" alt="Market Research">
                                <div class="wpo-service-single-content-des">
<h2>How To Creat a Great Company With Strategy and Planning</h2>
<p>I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful.</p>
<p>Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise.</p>
                                    <div class="wpo-service-single-sub-img">
                                        <ul>
                                            <li><img src="assets/themes/consultar/assets/images/service-single/2.jpg" alt=""></li>
                                            <li><img src="assets/themes/consultar/assets/images/service-single/3.jpg" alt=""></li>
                                        </ul>
                                    </div>  
                                </div>
                            </div>                        
                            <div class="wpo-solutions-section">
                                <h2>Our Solutions</h2>
                                <div class="row">
                                                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="wpo-solutions-item">
                            <div class="wpo-solutions-icon">
                                <div class="icon">
                                    <img src="assets/themes/consultar/assets/images/icon/briefcase.svg" alt="">
                                </div>
                            </div>
                            <div class="wpo-solutions-text">
                                <h2><a href="corporate-finance-s.html">Corporate Finance</a></h2>
                                <p>One way to categorize the activities is in terms of the professional’s area of expertise such as competitive analysis, corporate strategy.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="wpo-solutions-item">
                            <div class="wpo-solutions-icon">
                                <div class="icon">
                                    <img src="assets/themes/consultar/assets/images/icon/chart.svg" alt="">
                                </div>
                            </div>
                            <div class="wpo-solutions-text">
                                <h2><a href="market-research-s.html">Market Research</a></h2>
                                <p>One way to categorize the activities is in terms of the professional’s area of expertise such as competitive analysis, corporate strategy.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="wpo-solutions-item">
                            <div class="wpo-solutions-icon">
                                <div class="icon">
                                    <img src="assets/themes/consultar/assets/images/icon/badge.svg" alt="">
                                </div>
                            </div>
                            <div class="wpo-solutions-text">
                                <h2><a href="business-analysis.html">Business Analysis</a></h2>
                                <p>One way to categorize the activities is in terms of the professional’s area of expertise such as competitive analysis, corporate strategy.</p>
                            </div>
                        </div>
                    </div> 
                                </div>
                            </div>
                             
                            <div class="wpo-benefits-section">
                                <h2>Benefits</h2>
                                <div class="row">
                                    <div class="col-lg-12 col-12">
                                        <div class="wpo-benefits-item">
                                            <div class="accordion" id="accordionExample">
                                                               
                                                <div class="accordion-item">
                                                  <h3 class="accordion-header" id="heading1">
                                                    <button class="accordion-button " type="button" data-bs-toggle="collapse" data-bs-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                                        Market research on our global panel with support from our experts.
                                                    </button>
                                                  </h3>
                                                  <div id="collapse1" class="accordion-collapse collapse show" aria-labelledby="heading1" data-bs-parent="#accordionExample">
                                                    <div class="accordion-body">
                                                       <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eum exercitationem pariatur iure nemo esse repellendus est quo recusandae. Delectus, maxime.</p>
                                                    </div>
                                                  </div>
                                                </div>                                                               
                                                <div class="accordion-item">
                                                  <h3 class="accordion-header" id="heading2">
                                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                                        Planning can help alleviate workplace stress and increase productivity.
                                                    </button>
                                                  </h3>
                                                  <div id="collapse2" class="accordion-collapse collapse " aria-labelledby="heading2" data-bs-parent="#accordionExample">
                                                    <div class="accordion-body">
                                                       <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eum exercitationem pariatur iure nemo esse repellendus est quo recusandae. Delectus, maxime.</p>
                                                    </div>
                                                  </div>
                                                </div>                                                               
                                                <div class="accordion-item">
                                                  <h3 class="accordion-header" id="heading3">
                                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                                        Those who experiment the most, are able to innovate the best.
                                                    </button>
                                                  </h3>
                                                  <div id="collapse3" class="accordion-collapse collapse " aria-labelledby="heading3" data-bs-parent="#accordionExample">
                                                    <div class="accordion-body">
                                                       <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eum exercitationem pariatur iure nemo esse repellendus est quo recusandae. Delectus, maxime.</p>
                                                    </div>
                                                  </div>
                                                </div>                                                               
                                                <div class="accordion-item">
                                                  <h3 class="accordion-header" id="heading4">
                                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                                        Understand Your Problem, You must understand the issue.
                                                    </button>
                                                  </h3>
                                                  <div id="collapse4" class="accordion-collapse collapse " aria-labelledby="heading4" data-bs-parent="#accordionExample">
                                                    <div class="accordion-body">
                                                       <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eum exercitationem pariatur iure nemo esse repellendus est quo recusandae. Delectus, maxime.</p>
                                                    </div>
                                                  </div>
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-8">
                        <div class="wpo-single-sidebar">
                            <div class="wpo-service-widget widget">
                                 <h2>All Services</h2>
                                 <ul>
                                
                                     <li><a href="corporate-finance-s.html">Corporate Finance</a></li>

                                     <li><a href="market-research-s.html">Market Research</a></li>

                                     <li><a href="business-analysis.html">Business Analysis</a></li> 
                                 </ul>
                            </div>
<div class="wpo-newsletter-widget widget">
     <h2>Newsletter</h2>
     <p>Join 20,000 Sabscribers!</p>
     
      
        <form class="form-box form-ajax form-inline align-center ajax_form af_example form" method="post" action="https://consultar.webps.info/market-research-s.html">
            <input class="input-text" name="email" type="email" placeholder="Email Address" value="">
            <input type="submit" name="contact" class="sub_btn" value="Sing Up">
            <span class="form-message"></span>
        </form>
     <span>By signing up you agree to our <a href="about.html">Privecy Policy</a></span>
</div>                           
                            <div class="wpo-instagram-widget widget">
                                 <h2>Instagram Shot</h2>
                                 <ul>
                                     <li><img src="assets/themes/consultar/assets/images/instragram/7.jpg" alt=""></li>
                                     <li><img src="assets/themes/consultar/assets/images/instragram/6.jpg" alt=""></li>
                                     <li><img src="assets/themes/consultar/assets/images/instragram/4.jpg" alt=""></li>
                                     <li><img src="assets/themes/consultar/assets/images/instragram/5.jpg" alt=""></li>
                                     <li><img src="assets/themes/consultar/assets/images/instragram/3.jpg" alt=""></li>
                                     <li><img src="assets/themes/consultar/assets/images/instragram/2.jpg" alt=""></li>
                                 </ul>
                            </div>     
                            
                            <div class="wpo-contact-widget widget">
                                 <h2>How We Can <br> Help You!</h2>
                                 <p>labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. </p>
                                 <a href="contacts.html">Contact Us</a>
                            </div>      
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end of wpo-service-single-section -->


        <!-- start of wpo-site-footer-section -->
        <footer class="wpo-site-footer">
            <div class="wpo-upper-footer">
                <div class="container">
                    <div class="row">
                        <div class="col col-lg-3 col-md-6 col-sm-12 col-12">
                            <div class="widget about-widget">
                                <div class="logo widget-title">
                                    <img src="assets/themes/consultar/assets/images/logo2.png" alt="Consultar - Consulting Business MODX Theme">
                                </div>
                                <p>Management consulting includes a broad range of activities, and the many firms and their members often define these practices.</p>        
                                <ul>
                                    <li><a target="_blank" href="https://facebook.com/"><i class="ti-facebook"></i></a></li>
                                    <li><a target="_blank" href="https://twitter.com/"><i class="ti-twitter-alt"></i></a></li>
                                    <li><a target="_blank" href="https://www.instagram.com/"><i class="ti-instagram"></i></a></li>
                                    <li><a target="_blank" href="https://www.google.com/"><i class="ti-google"></i></a></li> 
                                </ul>
                            </div>
                        </div>
                        <div class="col col-lg-3 col-md-6 col-sm-12 col-12">
                            <div class="widget wpo-service-link-widget">
                                <div class="widget-title">
                                    <h3>Contact </h3>
                                </div>
                                <div class="contact-ft">
                                    <ul>                                        
                                <li><i class="fi flaticon-location"></i>7 Green Lake Street Crawfordsville, IN 47933</li>                                        
                                <li><i class="fi flaticon-send"></i>consultar@mail.com <br>  helloyou@mail.com</li>                                        
                                <li><i class="fi flaticon-phone-call"></i>+1 800 123 456 789 <br>  +1 800 123 654 987</li>
                                    </ul>   
                                </div>
                            </div>
                        </div>
                        <div class="col col-lg-3 col-md-6 col-sm-12 col-12">
                            <div class="widget link-widget">
                                <div class="widget-title">
                                    <h3>Services </h3>
                                </div>
                                <ul>
                                    <li><a href="corporate-finance-s.html">Corporate Finance</a></li>
                                    <li><a href="market-research-s.html">Market Research</a></li>
                                    <li><a href="business-analysis.html">Business Analysis</a></li>
                                    <li><a href="consumer-markets.html">Consumer Markets</a></li>
                                    <li><a href="insurance.html">Insurance</a></li> 
                                </ul>
                            </div>
                        </div>
                        
                        <div class="col col-lg-3 col-md-6 col-sm-12 col-12">
                            <div class="widget instagram">
                                <div class="widget-title">
                                    <h3>Projects</h3>
                                </div>
                                <ul class="d-flex">
                                    <li><a href="digital-analysis.html"><img src="assets/components/phpthumbof/cache/img-6.16326573b3bd77d12f367d9c7c01882a.jpg" alt="Digital Analysis"></a></li>    
                                    <li><a href="corporate-finance.html"><img src="assets/components/phpthumbof/cache/img-7.16326573b3bd77d12f367d9c7c01882a.jpg" alt="Corporate Finance"></a></li>    
                                    <li><a href="market-research.html"><img src="assets/components/phpthumbof/cache/img-8.16326573b3bd77d12f367d9c7c01882a.jpg" alt="Market Research"></a></li>    
                                    <li><a href="business-analysis-project.html"><img src="assets/components/phpthumbof/cache/img-9.16326573b3bd77d12f367d9c7c01882a.jpg" alt="Business Analysis"></a></li>    
                                    <li><a href="investment-strategy.html"><img src="assets/components/phpthumbof/cache/img-10.16326573b3bd77d12f367d9c7c01882a.jpg" alt="Investment Strategy"></a></li>    
                                    <li><a href="consumer-markets-project.html"><img src="assets/components/phpthumbof/cache/img-11.16326573b3bd77d12f367d9c7c01882a.jpg" alt="Consumer Markets"></a></li>     
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> <!-- end container -->
            </div>
            <div class="wpo-lower-footer">
                <div class="container">
                    <div class="row">
                        <div class="col col-xs-12">
                            <p class="copyright"> Copyright &copy; 2022 Consultar by <a href="https://themeforest.net/user/webpsdev/portfolio" target="_blank">WebPSdev</a> & <a href="https://themeforest.net/user/wpoceans" target="_blank">wpoceans</a>. All Rights Reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end of wpo-site-footer-section -->
    </div>
    <!-- end of page-wrapper -->
    <!-- All JavaScript files
    ================================================== -->
    <script src="{{url('assets/themes/consultar/assets/js/jquery.min.js') }}"></script>
    <script src="{{url('assets/themes/consultar/assets/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Plugins for this template -->
    <script src="{{url('assets/themes/consultar/assets/js/modernizr.custom.js') }}"></script>
    <script src="{{url('assets/themes/consultar/assets/js/jquery.dlmenu.js') }}"></script>
    <script src="{{url('assets/themes/consultar/assets/js/jquery-plugin-collection.js') }}"></script>
    <!-- Custom script for this template -->
    <script src="{{url('assets/themes/consultar/assets/js/script.js') }}"></script>
    <!-- Share buttons -->
    <script type="text/javascript" src="{{ url('s7.addthis.com/js/300/addthis_widget.js#pubid=ra-62013f10644665a7') }}"></script>
</body>


<!-- Mirrored from consultar.webps.info/market-research-s.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Jul 2022 16:34:22 GMT -->
</html>