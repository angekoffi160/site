<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('acceuil');

Route::get('/propos/d', function(){
    return view('about');
})->name('propos');

Route::get('/services/p', function(){
    return view('services');
})->name('services');

Route::get('/projet/p', function(){
    return view('projects');
})->name('projet');

Route::get('/pricing/p', function(){
    return view('pricing');
})->name('pricing');

Route::get('/testimonial/p', function(){
    return view('testimonial');
})->name('testimonial');
Route::get('/blog/p', function(){
    return view('blog');
})->name('blog');
Route::get('/about/p', function(){
    return view('contacts');
})->name('contact');
