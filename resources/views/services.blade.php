@extends('layouts.ditech_master')
@section('content')

        <!-- start of breadcumb-section -->
        <div class="wpo-breadcumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-breadcumb-wrap">
                            <h2>Our service</h2>
                            <ul id="breadcrumb" itemprop="breadcrumb"><li><a href="index.html">Home</a></li>
                                <li><span>Services</span></li></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of wpo-breadcumb-section-->
        <!-- start of wpo-service-section -->
        <section class="wpo-service-section section-padding">
            <div class="container">
                <div class="row">
                                            <div class="col-lg-4 col-md-6 col-12">
                            <div class="wpo-service-item">
                                <div class="wpo-service-icon">
                                    <div class="icon">
                                        <img src="assets/themes/consultar/assets/images/icon/briefcase.svg" alt="">
                                    </div>
                                </div>
                                <div class="wpo-service-text">
                                    <h2><a href="corporate-finance-s.html">Corporate Finance</a></h2>
                                    <p>One way to categorize the activities is in terms of the professional’s area of expertise such as competitive analysis, corporate strategy.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="wpo-service-item">
                                <div class="wpo-service-icon">
                                    <div class="icon">
                                        <img src="assets/themes/consultar/assets/images/icon/chart.svg" alt="">
                                    </div>
                                </div>
                                <div class="wpo-service-text">
                                    <h2><a href="market-research-s.html">Market Research</a></h2>
                                    <p>One way to categorize the activities is in terms of the professional’s area of expertise such as competitive analysis, corporate strategy.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="wpo-service-item">
                                <div class="wpo-service-icon">
                                    <div class="icon">
                                        <img src="assets/themes/consultar/assets/images/icon/badge.svg" alt="">
                                    </div>
                                </div>
                                <div class="wpo-service-text">
                                    <h2><a href="business-analysis.html">Business Analysis</a></h2>
                                    <p>One way to categorize the activities is in terms of the professional’s area of expertise such as competitive analysis, corporate strategy.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="wpo-service-item">
                                <div class="wpo-service-icon">
                                    <div class="icon">
                                        <img src="assets/themes/consultar/assets/images/icon/goal.svg" alt="">
                                    </div>
                                </div>
                                <div class="wpo-service-text">
                                    <h2><a href="consumer-markets.html">Consumer Markets</a></h2>
                                    <p>One way to categorize the activities is in terms of the professional’s area of expertise such as competitive analysis, corporate strategy.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="wpo-service-item">
                                <div class="wpo-service-icon">
                                    <div class="icon">
                                        <img src="assets/themes/consultar/assets/images/icon/handshake.svg" alt="">
                                    </div>
                                </div>
                                <div class="wpo-service-text">
                                    <h2><a href="insurance.html">Insurance</a></h2>
                                    <p>One way to categorize the activities is in terms of the professional’s area of expertise such as competitive analysis, corporate strategy.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="wpo-service-item">
                                <div class="wpo-service-icon">
                                    <div class="icon">
                                        <img src="assets/themes/consultar/assets/images/icon/clipboard.svg" alt="">
                                    </div>
                                </div>
                                <div class="wpo-service-text">
                                    <h2><a href="strategy-and-planning.html">Strategy and Planning</a></h2>
                                    <p>One way to categorize the activities is in terms of the professional’s area of expertise such as competitive analysis, corporate strategy.</p>
                                </div>
                            </div>
                        </div> 
                </div>
            </div>
        </section>
        <!-- end of wpo-service-section -->
@endsection()