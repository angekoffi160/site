@extends('layouts.ditech_master')
@section('content')
        <!-- start of breadcumb-section -->
        <div class="wpo-breadcumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-breadcumb-wrap">
                            <h2>Blog</h2>
                            <ul id="breadcrumb" itemprop="breadcrumb"><li><a href="index.html">Home</a></li>
                                <li><span>Blog</span></li></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of wpo-breadcumb-section-->
        <!-- start wpo-blog-pg-section -->
        <section class="wpo-blog-pg-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-8">
                        <div class="wpo-blog-content">
                                                        <div class="post format-standard-image">
                                <div class="entry-media">
                                    <img src="assets/themes/consultar/assets/images/blog/img-6.jpg" alt="Post Four">
                                </div>
                                <div class="entry-meta">
                                    <ul>
                                        <li><i class="fi flaticon-user"></i> By <a >Garry Watson</a> </li>
                                        <li><i class="fi flaticon-calendar"></i> 10 Apr 2022</li>
                                    </ul>
                                </div>
                                <div class="entry-details">
                                    <h3><a href="post-four.html">Why Choose Management Consulting?</a></h3>
                                    <p><p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first&#8230;</p></p>
                                    <a href="post-four.html" class="read-more">READ MORE...</a>
                                </div>
                            </div>
                            <div class="post format-standard-image">
                                <div class="entry-media">
                                    <img src="assets/themes/consultar/assets/images/blog/img-1.jpg" alt="Post Two">
                                </div>
                                <div class="entry-meta">
                                    <ul>
                                        <li><i class="fi flaticon-user"></i> By <a >Linda Watson</a> </li>
                                        <li><i class="fi flaticon-calendar"></i> 12 Mar 2022</li>
                                    </ul>
                                </div>
                                <div class="entry-details">
                                    <h3><a href="post-two.html">Tips From Successful Small Business Owners</a></h3>
                                    <p><p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first&#8230;</p></p>
                                    <a href="post-two.html" class="read-more">READ MORE...</a>
                                </div>
                            </div>
                            <div class="post format-standard-image">
                                <div class="entry-media">
                                    <img src="assets/themes/consultar/assets/images/blog/img-5.jpg" alt="Post Three">
                                </div>
                                <div class="entry-meta">
                                    <ul>
                                        <li><i class="fi flaticon-user"></i> By <a >Garry Watson</a> </li>
                                        <li><i class="fi flaticon-calendar"></i> 11 Mar 2022</li>
                                    </ul>
                                </div>
                                <div class="entry-details">
                                    <h3><a href="post-three.html">Worst Ways Small Businesses Waste Money on Marketing</a></h3>
                                    <p><p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first&#8230;</p></p>
                                    <a href="post-three.html" class="read-more">READ MORE...</a>
                                </div>
                            </div>  
							<div class="pagination-wrapper pagination-wrapper-left"><ul class="pg-pagination"><li class="active"><a href="blog.html">1</a></li>
<li><a href="blog4658.html?page=2">2</a></li><li><a href="blog4658.html?page=2"><i class="fi ti-angle-right"></i></a></li></ul></div>
                        </div>
                    </div>
                    <div class="col col-lg-4 col-12">
                        <div class="blog-sidebar">
                            <div class="widget search-widget">
                                                                <form role="search" action="https://consultar.webps.info/search.html" method="get" id="searchform" class="form">
                                    <div>
                                        <input type="text" class="form-control" name="search" placeholder="Search Post..">
                                        <button type="submit" class="btn"><i class="ti-search"></i></button>
                                        <input type="hidden" name="id" value="16" />
                                    </div>
                                </form>
                            </div>
                            <div class="widget category-widget">
                                <h3>Categories</h3>
                                <ul>
                                    <li><a href="search-by-categoryea96.html?tag=Business" title="Business">Business</a></li>
                                    <li><a href="search-by-category96f3.html?tag=Planning" title="Planning">Planning</a></li>
                                    <li><a href="search-by-category53c6.html?tag=Idea" title="Idea">Idea</a></li>
                                    <li><a href="search-by-category6cf4.html?tag=Strategy" title="Strategy">Strategy</a></li>
                                    <li><a href="search-by-categorybd9f.html?tag=Achievement" title="Achievement">Achievement</a></li>
                                    <li><a href="search-by-categoryad99.html?tag=Consulting" title="Consulting">Consulting</a></li>
                                    <li><a href="search-by-categoryab24.html?tag=IT" title="IT">IT</a></li>
                                </ul>
                            </div>
                            <div class="widget recent-post-widget">
                                <h3>Latest Posts</h3>
                                <div class="posts">
                                    
                                    <div class="post">
                                        <div class="img-holder">
                                            <img src="assets/components/phpthumbof/cache/img-6.5121f4ae312ef534de84c646d04353d0.jpg" alt>
                                        </div>
                                        <div class="details">
                                            <h4><a href="post-four.html">Why Choose Management Consulting?</a></h4>
                                            <span class="date">10 Apr 2022 </span>
                                        </div>
                                    </div>

                                    <div class="post">
                                        <div class="img-holder">
                                            <img src="assets/components/phpthumbof/cache/img-1.5121f4ae312ef534de84c646d04353d0.jpg" alt>
                                        </div>
                                        <div class="details">
                                            <h4><a href="post-two.html">Tips From Successful Small Business Owners</a></h4>
                                            <span class="date">12 Mar 2022 </span>
                                        </div>
                                    </div>

                                    <div class="post">
                                        <div class="img-holder">
                                            <img src="assets/components/phpthumbof/cache/img-5.5121f4ae312ef534de84c646d04353d0.jpg" alt>
                                        </div>
                                        <div class="details">
                                            <h4><a href="post-three.html">Worst Ways Small Businesses Waste Money on Marketing</a></h4>
                                            <span class="date">11 Mar 2022 </span>
                                        </div>
                                    </div>

                                    <div class="post">
                                        <div class="img-holder">
                                            <img src="assets/components/phpthumbof/cache/img-4.5121f4ae312ef534de84c646d04353d0.jpg" alt>
                                        </div>
                                        <div class="details">
                                            <h4><a href="post-one.html">8 Mistakes Founders Make When Starting a Business</a></h4>
                                            <span class="date">26 Feb 2022 </span>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                            <div class="widget wpo-instagram-widget">
                                <div class="widget-title">
                                    <h3>Projects</h3>
                                </div>
                                    <ul class="d-flex">
                                    
                                    <li><a href="digital-analysis.html"><img src="assets/components/phpthumbof/cache/img-6.968c9ec307cafb9c1a04bf6a072e4633.jpg" alt="Digital Analysis"></a></li>

                                    <li><a href="corporate-finance.html"><img src="assets/components/phpthumbof/cache/img-7.968c9ec307cafb9c1a04bf6a072e4633.jpg" alt="Corporate Finance"></a></li>

                                    <li><a href="market-research.html"><img src="assets/components/phpthumbof/cache/img-8.968c9ec307cafb9c1a04bf6a072e4633.jpg" alt="Market Research"></a></li>

                                    <li><a href="business-analysis-project.html"><img src="assets/components/phpthumbof/cache/img-9.968c9ec307cafb9c1a04bf6a072e4633.jpg" alt="Business Analysis"></a></li>

                                    <li><a href="investment-strategy.html"><img src="assets/components/phpthumbof/cache/img-10.968c9ec307cafb9c1a04bf6a072e4633.jpg" alt="Investment Strategy"></a></li>

                                    <li><a href="consumer-markets-project.html"><img src="assets/components/phpthumbof/cache/img-11.968c9ec307cafb9c1a04bf6a072e4633.jpg" alt="Consumer Markets"></a></li> 
                                </ul>
                            </div>
                            <div class="widget tag-widget">
                                <h3>Tags</h3>
                                <ul>
                                    <li><a href="search-by-tags96f3.html?tag=Planning" title="Planning">Planning</a></li>
                                    <li><a href="search-by-tags53c6.html?tag=Idea" title="Idea">Idea</a></li>
                                    <li><a href="search-by-tagsad99.html?tag=Consulting" title="Consulting">Consulting</a></li>
                                    <li><a href="search-by-tags6cf4.html?tag=Strategy" title="Strategy">Strategy</a></li>
                                    <li><a href="search-by-tagsea96.html?tag=Business" title="Business">Business</a></li>
                                    <li><a href="search-by-tagsbd9f.html?tag=Achievement" title="Achievement">Achievement</a></li> 
                                </ul>
                            </div>    
                            
                            <div class="wpo-contact-widget widget">
                                 <h2>How We Can <br> Help You!</h2>
                                 <p>labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. </p>
                                 <a href="contacts.html">Contact Us</a>
                            </div>  
                        </div>
                    </div>                    
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end wpo-blog-pg-section -->

    @endsection()