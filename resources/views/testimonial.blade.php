@extends('layouts.ditech_master')
@section('content')

        <!-- start of breadcumb-section -->
        <div class="wpo-breadcumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-breadcumb-wrap">
                            <h2>Testimonial</h2>
                            <ul id="breadcrumb" itemprop="breadcrumb"><li><a href="index.html">Home</a></li>
                                <li><span>Testimonial</span></li></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of wpo-breadcumb-section-->

         <!--Start wpo-testimonial-section-->  
         <section class="wpo-testimonial-section style-2 section-padding">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-12">
                        <div class="wpo-testimonial-title">
                            <h2><i class="fi flaticon-left-quote"></i> What <span>Our Clients</span> are Saying</h2>
                        </div>
                    </div>
                </div>
                <div class="wpo-testimonial-items wpo-testimonial-slider owl-carousel">
                    <div class="wpo-testimonial-item">
                        <div class="wpo-testimonial-avatar">
                           <img src="assets/themes/consultar/assets/images/testimonial/img-1.jpg" alt="Testimonial">
                        </div>
                        <div class="wpo-testimonial-text">
                            <p>I just wanted to share a quick note and let you know that you guys do a really good job. I’m glad I decided to work with you. It’s really great how easy.</p>
                            <div class="wpo-testimonial-text-btm">
                                <h3>Jenny Watson</h3>
                                <span>SCG First Company</span>
                            </div>
                        </div>
                    </div>                    <div class="wpo-testimonial-item">
                        <div class="wpo-testimonial-avatar">
                           <img src="assets/themes/consultar/assets/images/testimonial/img-2.jpg" alt="Testimonial">
                        </div>
                        <div class="wpo-testimonial-text">
                            <p>I just wanted to share a quick note and let you know that you guys do a really good job. I’m glad I decided to work with you. It’s really great how easy.</p>
                            <div class="wpo-testimonial-text-btm">
                                <h3>Harry Abraham</h3>
                                <span>SCG First Company</span>
                            </div>
                        </div>
                    </div>                    <div class="wpo-testimonial-item">
                        <div class="wpo-testimonial-avatar">
                           <img src="assets/themes/consultar/assets/images/testimonial/img-3.jpg" alt="Testimonial">
                        </div>
                        <div class="wpo-testimonial-text">
                            <p>I just wanted to share a quick note and let you know that you guys do a really good job. I’m glad I decided to work with you. It’s really great how easy.</p>
                            <div class="wpo-testimonial-text-btm">
                                <h3>Ron Di-soza</h3>
                                <span>SCG First Company</span>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </section>
        <!--End wpo-testimonial-section-->
@endsection()