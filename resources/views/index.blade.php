<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from consultar.webps.info/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Jul 2022 16:33:56 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="" />
	<meta name="description" content="We bring the right people together.">
	<meta name="author" content="Admin">    
    <link rel="shortcut icon" type="image/png" href="assets/themes/consultar/assets/images/favicon.png">
    <title>Home | Consultar - Consulting Business MODX Theme</title>
    <link href="{{ url('assets/themes/consultar/assets/css/themify-icons.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/flaticon.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/animate.css')}}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/owl.theme.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/slick.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/slick-theme.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/swiper.min.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/owl.transitions.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/jquery.fancybox.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/odometer-theme-default.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/component.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/responsive.css') }}" rel="stylesheet">
</head>


<body>
    <!-- start page-wrapper -->
    <div class="page-wrapper">
        <!-- start preloader -->
        <div class="preloader">
            <div class="angular-shape">
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div class="spinner">
              <div class="double-bounce1"></div>
              <div class="double-bounce2"></div>
            </div>
        </div>
        <!-- end preloader -->
        <!-- Start header -->
        <header id="header" class="wpo-site-header">
            <nav class="navigation navbar navbar-expand-lg navbar-light">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-lg-3 col-md-3 col-6">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggler open-btn">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar first-angle"></span>
                                    <span class="icon-bar middle-angle"></span>
                                    <span class="icon-bar last-angle"></span>
                                </button>
                                <a class="navbar-brand"><img src="assets/themes/consultar/assets/images/logo.png" alt=""></a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-2 col-1">
                            <div id="navbar" class="collapse navbar-collapse navigation-holder">
                                <button class="close-navbar"><i class="ti-close"></i></button>
                                <ul class="nav navbar-nav mb-2 mb-lg-0">
                                    <li class="active">
    									<a class="active" href="#">
    										Home
    									</a>
    								</li>
                                    <li>
        								<a href="about.html">
        									About
        								</a>
        							</li>
                                    <li>
        								<a href="services.html">
        									Services
        								</a>
        							</li>
                                    <li>
        								<a href="projects.html">
        									Projects
        								</a>
        							</li>
                                        <li class="menu-item-has-children">
    									<a href="#">
    											Pages
    										</a>
    										<ul class="sub-menu">
                                    <li>
        								<a href="pricing.html">
        									Pricing
        								</a>
        							</li>
                                    <li>
        								<a href="testimonial.html">
        									Testimonial
        								</a>
        							</li>

                                            </ul>
    									</li>
											
                                    <li>
        								<a href="blog.html">
        									Blog
        								</a>
        							</li>
                                    <li>
        								<a href="contacts.html">
        									Contacts
        								</a>
        							</li>

                                </ul>
                                <div id="dl-menu" class="dl-menuwrapper">
                                    <button class="dl-trigger">Open Menu</button>
                                    <ul class="dl-menu">
                                    <li class="active">
    									<a class="active" href="#">
    										Home
    									</a>
    								</li>
                                    <li>
        								<a href="about.html">
        									About
        								</a>
        							</li>
                                    <li>
        								<a href="services.html">
        									Services
        								</a>
        							</li>
                                    <li>
        								<a href="projects.html">
        									Projects
        								</a>
        							</li>
                                        <li>
    									<a href="#">
    											Pages
    										</a>
    										<ul class="dl-submenu">
                                    <li>
        								<a href="pricing.html">
        									Pricing
        								</a>
        							</li>
                                    <li>
        								<a href="testimonial.html">
        									Testimonial
        								</a>
        							</li>

                                            </ul>
    									</li>
											
                                    <li>
        								<a href="blog.html">
        									Blog
        								</a>
        							</li>
                                    <li>
        								<a href="contacts.html">
        									Contacts
        								</a>
        							</li>

                                    </ul>
                                </div><!-- /dl-menuwrapper -->
                            </div><!-- end of nav-collapse -->
                        </div>
                        <div class="col-lg-3 col-md-4 col-4">
                            <div class="header-right">
                                <div class="close-form">
                                    <a target="_blank" class="theme-btn" href="contacts.html">Free Consulting</a>
                                </div>
                                <div class="header-search-form-wrapper">
                                    <div class="cart-search-contact">
                                        <button class="search-toggle-btn"><i class="fi flaticon-magnifiying-glass"></i></button>
                                        <div class="header-search-form">
                                            <form role="search" action="https://consultar.webps.info/search.html" method="get" id="searchform"  class="form">
                                                <div>
                                                    <input type="text" class="form-control" name="search" placeholder="Search here...">
                                                    <button  type="submit" class="btn"><i class="fi flaticon-magnifiying-glass"></i></button>
                                                    <input type="hidden" name="id" value="16" />
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end of container -->
            </nav>
        </header>
        <!-- end of header -->
        <!-- start of wpo-hero-section-1 -->
        <section class="wpo-hero-section-1 wpo-hero-section-2">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-6 col-lg-6">
                        <div class="wpo-hero-section-text">
                            <div class="wpo-hero-title-top">
                                <span>We bring the right people together.</span>
                            </div>
                            <div class="wpo-hero-title">
                                <h2>Helping You Solve Your Problems</h2>
                            </div>
                            <div class="wpo-hero-subtitle">
                                <p>Facilitating client learning—that is, teaching clients how to resolve similar problems in the future.</p>
                            </div>
                            <div class="btns">
                                <a href="services.html" class="btn theme-btn">Get Started</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right-vec">
                <div class="right-img">
                    <div class="r-img">
                        <img src="assets/themes/consultar/assets/images/slider/right-img2.png" alt="">
                        <!-- <div class="svg-shape">
                            <svg width="826" height="309" viewBox="0 0 826 309" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M100.983 35.4286C112.365 13.6512 134.903 0 159.476 0H754.544C785.255 0 811.908 21.1833 818.841 51.102L823.663 71.9141C825.212 78.5972 825.709 85.4808 825.136 92.317L812.063 248.505C809.201 282.702 780.61 309 746.293 309H66.9661C17.3837 309 -14.4933 256.371 8.4734 212.429L100.983 35.4286Z" fill="#edf2f8"/>
                             </svg>
                        </div> -->
                        <div class="pop-up-video">
                            <div class="video-holder">
                                <a href="https://www.youtube.com/embed/7Jv48RQ_2gk" class="video-btn" data-type="iframe"><i class="fi flaticon-play"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="wpo-happy-client">
                          <div class="wpo-happy-client-text">
                              <h3>252+</h3>
                              <p>Happy Clients</p>
                          </div>
                          <div class="wpo-happy-client-img">
                              <ul class="wpo-happy-client-slide owl-carousel">
                                  <li><img src="assets/components/phpthumbof/cache/client1.ed6edb87b8d1919f76f9014e3f81af1e.png" alt=""></li>
                                  <li><img src="assets/components/phpthumbof/cache/client2.ed6edb87b8d1919f76f9014e3f81af1e.png" alt=""></li>
                                  <li><img src="assets/components/phpthumbof/cache/client3.ed6edb87b8d1919f76f9014e3f81af1e.png" alt=""></li>
                                  <li><img src="assets/components/phpthumbof/cache/client4.ed6edb87b8d1919f76f9014e3f81af1e.png" alt=""></li>
                              </ul>
                          </div>
                    </div>   
                </div>
            </div>
        </section>
        <!-- end of wpo-hero-section-1 slider -->
        <!-- start of wpo-features-section -->
        <section class="wpo-features-section section-padding">
            <div class="container">
                <div class="row">                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="wpo-features-item">
                            <div class="wpo-features-icon">
                                <div class="icon">
                                    <img src="assets/themes/consultar/assets/images/icon/document.svg" alt="">
                                </div>
                            </div>
                            <div class="wpo-features-text">
                                <h2><a>Strategy and Planning</a></h2>
                                <p>One way to categorize the activities is in terms of the professional’s area of expertise such as competitive analysis, corporate strategy.</p>
                            </div>
                        </div>
                    </div>                                        <div class="col-lg-4 col-md-6 col-12">
                        <div class="wpo-features-item">
                            <div class="wpo-features-icon">
                                <div class="icon">
                                    <img src="assets/themes/consultar/assets/images/icon/bar-graph.svg" alt="">
                                </div>
                            </div>
                            <div class="wpo-features-text">
                                <h2><a>Market Analysis</a></h2>
                                <p>One way to categorize the activities is in terms of the professional’s area of expertise such as competitive analysis, corporate strategy.</p>
                            </div>
                        </div>
                    </div>                                        <div class="col-lg-4 col-md-6 col-12">
                        <div class="wpo-features-item">
                            <div class="wpo-features-icon">
                                <div class="icon">
                                    <img src="assets/themes/consultar/assets/images/icon/clipboard.svg" alt="">
                                </div>
                            </div>
                            <div class="wpo-features-text">
                                <h2><a>Investment Strategy</a></h2>
                                <p>One way to categorize the activities is in terms of the professional’s area of expertise such as competitive analysis, corporate strategy.</p>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </section>   
        <!-- end of wpo-features-section -->

        <!-- start of wpo-about-section -->
        <section class="wpo-about-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-12">
                        <div class="wpo-about-wrap">
                           <div class="wpo-about-img">
                              <img src="assets/themes/consultar/assets/images/about.jpg" alt="About">
                              <div class="wpo-ab-shape-1"><img src="assets/themes/consultar/assets/images/ab-shape-1.png" alt="About"></div>
                              <div class="wpo-ab-shape-2"><img src="assets/themes/consultar/assets/images/ab-shape-2.png" alt="About"></div>
                           </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-12">
                        <div class="wpo-about-text">
                           <div class="wpo-about-icon">
                                <div class="icon">
                                    <img src="assets/themes/consultar/assets/images/icon/badge.svg" alt="About">
                                </div>
                           </div>
                           <div class="wpo-about-icon-content">
                               <h2>Professional And Dedicated <span>Consulting</span> Solutions</h2>
                               <p><p>Management consulting includes a broad range of activities, and the many firms and their members often define these practices quite differently. One way to categorize the activities is in terms of the professional&amp;rsquo;s area of expertise (such as competitive analysis, corporate strategy, operations management, or human resources). But in practice, as many differences exist within these categories as between them.</p></p>
                               <div class="signeture">
                                   <span><img src="assets/themes/consultar/assets/images/signeture.png" alt="About"></span>
                                   <p>Robert William, CEO</p>
                               </div>
                               <a class="theme-btn" href="about.html">More About</a>
                           </div>
                        </div>
                    </div>
                </div>
            </div>      
        </section>
        <!-- end of wpo-about-section -->
        <!-- start of wpo-service-section -->
        <section class="wpo-service-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-section-title">
                            <span>Our Services</span>
                            <h2>Explore Our Services</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                                            <div class="col-lg-4 col-md-6 col-12">
                            <div class="wpo-service-item">
                                <div class="wpo-service-icon">
                                    <div class="icon">
                                        <img src="assets/themes/consultar/assets/images/icon/briefcase.svg" alt="">
                                    </div>
                                </div>
                                <div class="wpo-service-text">
                                    <h2><a href="corporate-finance-s.html">Corporate Finance</a></h2>
                                    <p>One way to categorize the activities is in terms of the professional’s area of expertise such as competitive analysis, corporate strategy.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="wpo-service-item">
                                <div class="wpo-service-icon">
                                    <div class="icon">
                                        <img src="assets/themes/consultar/assets/images/icon/chart.svg" alt="">
                                    </div>
                                </div>
                                <div class="wpo-service-text">
                                    <h2><a href="market-research-s.html">Market Research</a></h2>
                                    <p>One way to categorize the activities is in terms of the professional’s area of expertise such as competitive analysis, corporate strategy.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="wpo-service-item">
                                <div class="wpo-service-icon">
                                    <div class="icon">
                                        <img src="assets/themes/consultar/assets/images/icon/badge.svg" alt="">
                                    </div>
                                </div>
                                <div class="wpo-service-text">
                                    <h2><a href="business-analysis.html">Business Analysis</a></h2>
                                    <p>One way to categorize the activities is in terms of the professional’s area of expertise such as competitive analysis, corporate strategy.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="wpo-service-item">
                                <div class="wpo-service-icon">
                                    <div class="icon">
                                        <img src="assets/themes/consultar/assets/images/icon/goal.svg" alt="">
                                    </div>
                                </div>
                                <div class="wpo-service-text">
                                    <h2><a href="consumer-markets.html">Consumer Markets</a></h2>
                                    <p>One way to categorize the activities is in terms of the professional’s area of expertise such as competitive analysis, corporate strategy.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="wpo-service-item">
                                <div class="wpo-service-icon">
                                    <div class="icon">
                                        <img src="assets/themes/consultar/assets/images/icon/handshake.svg" alt="">
                                    </div>
                                </div>
                                <div class="wpo-service-text">
                                    <h2><a href="insurance.html">Insurance</a></h2>
                                    <p>One way to categorize the activities is in terms of the professional’s area of expertise such as competitive analysis, corporate strategy.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="wpo-service-item">
                                <div class="wpo-service-icon">
                                    <div class="icon">
                                        <img src="assets/themes/consultar/assets/images/icon/clipboard.svg" alt="">
                                    </div>
                                </div>
                                <div class="wpo-service-text">
                                    <h2><a href="strategy-and-planning.html">Strategy and Planning</a></h2>
                                    <p>One way to categorize the activities is in terms of the professional’s area of expertise such as competitive analysis, corporate strategy.</p>
                                </div>
                            </div>
                        </div> 
                </div>
            </div>
        </section>
        <!-- end of wpo-service-section -->
        <!-- start of wpo-pricing-section -->
        <section class="wpo-pricing-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-section-title">
                            <span>Pricing</span>
                            <h2>Our Pricing Packages</h2>
                        </div>
                    </div>
                </div>
                <div class="wpo-pricing-items">
                    <div class="row">
                                                <div class="col col-lg-4 col-md-6 col-12">
                            <div class="wpo-pricing-item">
                                <h2>Basic Pack</h2>
                                <div class="price-icon">
                                    <i class="fi flaticon-medal"></i>
                                </div>
                                <ul>
                                    <li>24/7 Support available</li>
                                    <li>Home Consulting System</li>
                                    <li>30-Day analytics & insights</li>
                                    <li>Ultimate Features</li>
                                </ul>
                                <a class="theme-btn" href="contacts.html">Choose Plan</a>
                            </div>
                        </div>
                        <div class="col col-lg-4 col-md-6 col-12">
                            <div class="wpo-pricing-item">
                                <h2>Standard Pack</h2>
                                <div class="price-icon">
                                    <i class="fi flaticon-stats"></i>
                                </div>
                                <ul>
                                    <li>24/7 Support available</li>
                                    <li>Home Consulting System</li>
                                    <li>30-Day analytics & insights</li>
                                    <li>Ultimate Features</li>
                                </ul>
                                <a class="theme-btn" href="contacts.html">Choose Plan</a>
                            </div>
                        </div>
                        <div class="col col-lg-4 col-md-6 col-12">
                            <div class="wpo-pricing-item">
                                <h2>Exended Pack</h2>
                                <div class="price-icon">
                                    <i class="fi flaticon-trophy"></i>
                                </div>
                                <ul>
                                    <li>24/7 Support available</li>
                                    <li>Home Consulting System</li>
                                    <li>30-Day analytics & insights</li>
                                    <li>Ultimate Features</li>
                                </ul>
                                <a class="theme-btn" href="contacts.html">Choose Plan</a>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </section>
        <!-- end of wpo-pricing-section -->
        <!-- start wpo-fun-fact-section -->
        <section class="wpo-fun-fact-section wpo-fun-fact-section-s2">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <div class="wpo-fun-fact-grids clearfix">      

                            <div class="grid">
                                <div class="info">
                                    <h3><span class="odometer" data-count="500">00</span>+</h3>
                                    <p>Completed Cases</p>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="info">
                                    <h3><span class="odometer" data-count="25">00</span>+</h3>
                                    <p>Expert Consultants</p>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="info">
                                    <h3><span class="odometer" data-count="95">00</span>%</h3>
                                    <p>Client Satisfaction</p>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="info">
                                    <h3><span class="odometer" data-count="25">00</span>+</h3>
                                    <p>Award Winning</p>
                                </div>
                            </div>         
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>   
        <!-- end wpo-fun-fact-section -->
        <!--Start wpo-gallery-section-->  
        <section class="wpo-gallery-section section-padding">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-sm-8">
                        <div class="wpo-section-title">
                            <span>Projects</span>
                            <h2>Our Project Gallery</h2>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-4">
                        <div class="wpo-section-title-button">
                            <a href="projects.html">More Projects</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col col-xs-12 sortable-gallery">
                        <div class="wpo-gallery-container">
                            <div class="grid">
                            <div class="wpo-gallery-item">
                                <a href="digital-analysis.html">
                                    <img src="assets/themes/consultar/assets/images/gallery/img-6.jpg" alt class="img img-responsive">
                                    <div class="wpo-gallery-text">
                                        <h3>Digital Analysis</h3>
                                        <i class="ti-plus"></i>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="grid">
                            <div class="wpo-gallery-item">
                                <a href="corporate-finance.html">
                                    <img src="assets/themes/consultar/assets/images/gallery/img-7.jpg" alt class="img img-responsive">
                                    <div class="wpo-gallery-text">
                                        <h3>Corporate Finance</h3>
                                        <i class="ti-plus"></i>
                                    </div>
                                </a>
                            </div>
                        </div>
                            <div class="grid">
                                <div class="wpo-gallery-item">
                                    <a href="market-research.html">
                                        <img src="assets/themes/consultar/assets/images/gallery/img-8.jpg" alt class="img img-responsive">
                                        <div class="wpo-gallery-text">
                                            <h3>Market Research</h3>
                                            <i class="ti-plus"></i>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="wpo-gallery-item">
                                    <a href="business-analysis-project.html">
                                        <img src="assets/themes/consultar/assets/images/gallery/img-9.jpg" alt class="img img-responsive">
                                        <div class="wpo-gallery-text">
                                            <h3>Business Analysis</h3>
                                            <i class="ti-plus"></i>
                                        </div>
                                    </a>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div> <!-- end row -->
            </div>
        </section>
        <!--End wpo-gallery-section-->
        <!--Start wpo-testimonial-section-->  
        <section class="wpo-testimonial-section section-padding">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-12">
                        <div class="wpo-testimonial-title">
                            <h2><i class="fi flaticon-left-quote"></i> What <span>Our Clients</span> are Saying</h2>
                        </div>
                    </div>
                </div>
                <div class="wpo-testimonial-items wpo-testimonial-slider owl-carousel">
                    <div class="wpo-testimonial-item">
                        <div class="wpo-testimonial-avatar">
                           <img src="assets/themes/consultar/assets/images/testimonial/img-1.jpg" alt="Home">
                        </div>
                        <div class="wpo-testimonial-text">
                            <p>I just wanted to share a quick note and let you know that you guys do a really good job. I’m glad I decided to work with you. It’s really great how easy.</p>
                            <div class="wpo-testimonial-text-btm">
                                <h3>Jenny Watson</h3>
                                <span>SCG First Company</span>
                            </div>
                        </div>
                    </div>                    <div class="wpo-testimonial-item">
                        <div class="wpo-testimonial-avatar">
                           <img src="assets/themes/consultar/assets/images/testimonial/img-2.jpg" alt="Home">
                        </div>
                        <div class="wpo-testimonial-text">
                            <p>I just wanted to share a quick note and let you know that you guys do a really good job. I’m glad I decided to work with you. It’s really great how easy.</p>
                            <div class="wpo-testimonial-text-btm">
                                <h3>Harry Abraham</h3>
                                <span>SCG First Company</span>
                            </div>
                        </div>
                    </div>                    <div class="wpo-testimonial-item">
                        <div class="wpo-testimonial-avatar">
                           <img src="assets/themes/consultar/assets/images/testimonial/img-3.jpg" alt="Home">
                        </div>
                        <div class="wpo-testimonial-text">
                            <p>I just wanted to share a quick note and let you know that you guys do a really good job. I’m glad I decided to work with you. It’s really great how easy.</p>
                            <div class="wpo-testimonial-text-btm">
                                <h3>Ron Di-soza</h3>
                                <span>SCG First Company</span>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </section>
        <!--End wpo-testimonial-section-->
        <!--Start wpo-support-section-->  
        <section class="wpo-support-section">
            <div class="container">
                <div class="wpo-support-wrapper">
                    <div class="wpo-support-text">
                        <h2>Have You a Different Question?</h2>
                        <p>Please create a ticket to our support forum,a great place to learn, share, and troubleshoot. Get started.</p>
                    </div>
                    <div class="wpo-support-btn">
                        <a href="#">ASK SUPPORT TICKET</a>
                    </div>
                </div>
            </div>
        </section>  
        <!--End wpo-support-section-->
        <!-- start of wpo-blog-section -->
        <section class="wpo-blog-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-section-title">
                            <span>Blog</span>
                            <h2>Our Blog Packages</h2>
                        </div>
                    </div>
                </div>
                <div class="wpo-blog-items">
                    <div class="row">
                                                <div class="col col-lg-4 col-md-6 col-12">
                            <div class="wpo-blog-item">
                                <div class="wpo-blog-img">
                                    <img src="assets/components/phpthumbof/cache/img-6.d4f3f68dd1cfaa3f908fda8c6a102ce9.jpg" alt="Post Four">
                                </div>
                                <div class="wpo-blog-content">
                                    <div class="wpo-blog-content-top">
                                        <div class="wpo-blog-thumb">
                                            <a href="search-by-tags296a.html?tag=Planning&amp;key=tags"><span>Planning</span></a> 
                                            <a href="search-by-tags59ce.html?tag=Idea&amp;key=tags"><span>Idea</span></a> 
                                            <a href="search-by-tagscd3d.html?tag=Business&amp;key=tags"><span>Business</span></a>
                                        </div>
                                        <h2><a href="post-four.html">Why Choose Management Consulting?</a></h2>
                                    </div>
                                    <div class="wpo-blog-content-btm">
                                        <div class="wpo-blog-content-btm-left">
                                            <div class="wpo-blog-content-btm-left-img">
                                                <img src="assets/themes/consultar/assets/images/blog-details/author.jpg" alt="">
                                            </div>
                                            <div class="wpo-blog-content-btm-left-text">
                                                <h4><a>Garry Watson</a></h4>
                                                <span>Marketing Manager</span>
                                            </div>
                                        </div>
                                        <div class="wpo-blog-content-btm-right">
                                            <span>20 Feb, 2022</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col col-lg-4 col-md-6 col-12">
                            <div class="wpo-blog-item">
                                <div class="wpo-blog-img">
                                    <img src="assets/components/phpthumbof/cache/img-1.d4f3f68dd1cfaa3f908fda8c6a102ce9.jpg" alt="Post Two">
                                </div>
                                <div class="wpo-blog-content">
                                    <div class="wpo-blog-content-top">
                                        <div class="wpo-blog-thumb">
                                            <a href="search-by-tags59ce.html?tag=Idea&amp;key=tags"><span>Idea</span></a> 
                                            <a href="search-by-tags6b8e.html?tag=Strategy&amp;key=tags"><span>Strategy</span></a> 
                                            <a href="search-by-tags296a.html?tag=Planning&amp;key=tags"><span>Planning</span></a>
                                        </div>
                                        <h2><a href="post-two.html">Tips From Successful Small Business Owners</a></h2>
                                    </div>
                                    <div class="wpo-blog-content-btm">
                                        <div class="wpo-blog-content-btm-left">
                                            <div class="wpo-blog-content-btm-left-img">
                                                <img src="assets/themes/consultar/assets/images/blog-details/author.jpg" alt="">
                                            </div>
                                            <div class="wpo-blog-content-btm-left-text">
                                                <h4><a>Linda Watson</a></h4>
                                                <span>Marketing Manager</span>
                                            </div>
                                        </div>
                                        <div class="wpo-blog-content-btm-right">
                                            <span>20 Feb, 2022</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col col-lg-4 col-md-6 col-12">
                            <div class="wpo-blog-item">
                                <div class="wpo-blog-img">
                                    <img src="assets/components/phpthumbof/cache/img-5.d4f3f68dd1cfaa3f908fda8c6a102ce9.jpg" alt="Post Three">
                                </div>
                                <div class="wpo-blog-content">
                                    <div class="wpo-blog-content-top">
                                        <div class="wpo-blog-thumb">
                                            <a href="search-by-tags296a.html?tag=Planning&amp;key=tags"><span>Planning</span></a> 
                                            <a href="search-by-tags10f4.html?tag=Achievement&amp;key=tags"><span>Achievement</span></a> 
                                            <a href="search-by-tags59ce.html?tag=Idea&amp;key=tags"><span>Idea</span></a>
                                        </div>
                                        <h2><a href="post-three.html">Worst Ways Small Businesses Waste Money on Marketing</a></h2>
                                    </div>
                                    <div class="wpo-blog-content-btm">
                                        <div class="wpo-blog-content-btm-left">
                                            <div class="wpo-blog-content-btm-left-img">
                                                <img src="assets/themes/consultar/assets/images/blog-details/author.jpg" alt="">
                                            </div>
                                            <div class="wpo-blog-content-btm-left-text">
                                                <h4><a>Garry Watson</a></h4>
                                                <span>Marketing Manager</span>
                                            </div>
                                        </div>
                                        <div class="wpo-blog-content-btm-right">
                                            <span>20 Feb, 2022</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </section>
        
        <!-- end of wpo-blog-section -->

        <!-- start of wpo-site-footer-section -->
        <footer class="wpo-site-footer">
            <div class="wpo-upper-footer">
                <div class="container">
                    <div class="row">
                        <div class="col col-lg-3 col-md-6 col-sm-12 col-12">
                            <div class="widget about-widget">
                                <div class="logo widget-title">
                                    <img src="assets/themes/consultar/assets/images/logo2.png" alt="Consultar - Consulting Business MODX Theme">
                                </div>
                                <p>Management consulting includes a broad range of activities, and the many firms and their members often define these practices.</p>        
                                <ul>
                                    <li><a target="_blank" href="https://facebook.com/"><i class="ti-facebook"></i></a></li>
                                    <li><a target="_blank" href="https://twitter.com/"><i class="ti-twitter-alt"></i></a></li>
                                    <li><a target="_blank" href="https://www.instagram.com/"><i class="ti-instagram"></i></a></li>
                                    <li><a target="_blank" href="https://www.google.com/"><i class="ti-google"></i></a></li> 
                                </ul>
                            </div>
                        </div>
                        <div class="col col-lg-3 col-md-6 col-sm-12 col-12">
                            <div class="widget wpo-service-link-widget">
                                <div class="widget-title">
                                    <h3>Contact </h3>
                                </div>
                                <div class="contact-ft">
                                    <ul>                                        
                                <li><i class="fi flaticon-location"></i>7 Green Lake Street Crawfordsville, IN 47933</li>                                        
                                <li><i class="fi flaticon-send"></i>consultar@mail.com <br>  helloyou@mail.com</li>                                        
                                <li><i class="fi flaticon-phone-call"></i>+1 800 123 456 789 <br>  +1 800 123 654 987</li>
                                    </ul>   
                                </div>
                            </div>
                        </div>
                        <div class="col col-lg-3 col-md-6 col-sm-12 col-12">
                            <div class="widget link-widget">
                                <div class="widget-title">
                                    <h3>Services </h3>
                                </div>
                                <ul>
                                    <li><a href="corporate-finance-s.html">Corporate Finance</a></li>
                                    <li><a href="market-research-s.html">Market Research</a></li>
                                    <li><a href="business-analysis.html">Business Analysis</a></li>
                                    <li><a href="consumer-markets.html">Consumer Markets</a></li>
                                    <li><a href="insurance.html">Insurance</a></li> 
                                </ul>
                            </div>
                        </div>
                        
                        <div class="col col-lg-3 col-md-6 col-sm-12 col-12">
                            <div class="widget instagram">
                                <div class="widget-title">
                                    <h3>Projects</h3>
                                </div>
                                <ul class="d-flex">
                                    <li><a href="digital-analysis.html"><img src="assets/components/phpthumbof/cache/img-6.16326573b3bd77d12f367d9c7c01882a.jpg" alt="Digital Analysis"></a></li>    
                                    <li><a href="corporate-finance.html"><img src="assets/components/phpthumbof/cache/img-7.16326573b3bd77d12f367d9c7c01882a.jpg" alt="Corporate Finance"></a></li>    
                                    <li><a href="market-research.html"><img src="assets/components/phpthumbof/cache/img-8.16326573b3bd77d12f367d9c7c01882a.jpg" alt="Market Research"></a></li>    
                                    <li><a href="business-analysis-project.html"><img src="assets/components/phpthumbof/cache/img-9.16326573b3bd77d12f367d9c7c01882a.jpg" alt="Business Analysis"></a></li>    
                                    <li><a href="investment-strategy.html"><img src="assets/components/phpthumbof/cache/img-10.16326573b3bd77d12f367d9c7c01882a.jpg" alt="Investment Strategy"></a></li>    
                                    <li><a href="consumer-markets-project.html"><img src="assets/components/phpthumbof/cache/img-11.16326573b3bd77d12f367d9c7c01882a.jpg" alt="Consumer Markets"></a></li>     
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> <!-- end container -->
            </div>
            <div class="wpo-lower-footer">
                <div class="container">
                    <div class="row">
                        <div class="col col-xs-12">
                            <p class="copyright"> Copyright &copy; 2022 Consultar by <a href="https://themeforest.net/user/webpsdev/portfolio" target="_blank">WebPSdev</a> & <a href="https://themeforest.net/user/wpoceans" target="_blank">wpoceans</a>. All Rights Reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end of wpo-site-footer-section -->
    </div>
    <!-- end of page-wrapper -->
    <!-- All JavaScript files
    ================================================== -->
    <script src="{{url('assets/themes/consultar/assets/js/jquery.min.js') }}"></script>
    <script src="{{url('assets/themes/consultar/assets/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Plugins for this template -->
    <script src="{{url('assets/themes/consultar/assets/js/modernizr.custom.js') }}"></script>
    <script src="{{url('assets/themes/consultar/assets/js/jquery.dlmenu.js') }}"></script>
    <script src="{{url('assets/themes/consultar/assets/js/jquery-plugin-collection.js') }}"></script>
    <!-- Custom script for this template -->
    <script src="{{url('assets/themes/consultar/assets/js/script.js') }}"></script>
    <!-- Share buttons -->
    <script type="text/javascript" src="{{ url('s7.addthis.com/js/300/addthis_widget.js#pubid=ra-62013f10644665a7') }}"></script>
</body>


<!-- Mirrored from consultar.webps.info/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Jul 2022 16:34:05 GMT -->
</html>