@extends('layouts.ditech_master')
@section('content')

        <!-- start of breadcumb-section -->
        <div class="wpo-breadcumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-breadcumb-wrap">
                            <h2>Pricing</h2>
                            <ul id="breadcrumb" itemprop="breadcrumb"><li><a href="index.html">Home</a></li>
                                <li><span>Pricing</span></li></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of wpo-breadcumb-section-->

        <!-- start of wpo-pricing-section -->
        <section class="wpo-pricing-section section-padding">
            <div class="container">
                <div class="wpo-pricing-items">
                    <div class="row">
                                                <div class="col col-lg-4 col-md-6 col-12">
                            <div class="wpo-pricing-item">
                                <h2>Basic Pack</h2>
                                <div class="price-icon">
                                    <i class="fi flaticon-medal"></i>
                                </div>
                                <ul>
                                    <li>24/7 Support available</li>
                                    <li>Home Consulting System</li>
                                    <li>30-Day analytics & insights</li>
                                    <li>Ultimate Features</li>
                                </ul>
                                <a class="theme-btn" href="contacts.html">Choose Plan</a>
                            </div>
                        </div>
                        <div class="col col-lg-4 col-md-6 col-12">
                            <div class="wpo-pricing-item">
                                <h2>Standard Pack</h2>
                                <div class="price-icon">
                                    <i class="fi flaticon-stats"></i>
                                </div>
                                <ul>
                                    <li>24/7 Support available</li>
                                    <li>Home Consulting System</li>
                                    <li>30-Day analytics & insights</li>
                                    <li>Ultimate Features</li>
                                </ul>
                                <a class="theme-btn" href="contacts.html">Choose Plan</a>
                            </div>
                        </div>
                        <div class="col col-lg-4 col-md-6 col-12">
                            <div class="wpo-pricing-item">
                                <h2>Exended Pack</h2>
                                <div class="price-icon">
                                    <i class="fi flaticon-trophy"></i>
                                </div>
                                <ul>
                                    <li>24/7 Support available</li>
                                    <li>Home Consulting System</li>
                                    <li>30-Day analytics & insights</li>
                                    <li>Ultimate Features</li>
                                </ul>
                                <a class="theme-btn" href="contacts.html">Choose Plan</a>
                            </div>
                        </div> 
                    </div>
                </div>                            
            </div>
        </section>
        <!-- end of wpo-pricing-section -->


@endsection()