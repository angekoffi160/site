@extends('layouts.ditech_master')
@section('content')
        <!-- start of breadcumb-section -->
        <div class="wpo-breadcumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-breadcumb-wrap">
                            <h2>Projects</h2>
                            <ul id="breadcrumb" itemprop="breadcrumb"><li><a href="index.html">Home</a></li>
                                <li><span>Projects</span></li></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of wpo-breadcumb-section-->

        <!--Start wpo-project-section-->  
        <section class="wpo-gallery-section wpo-gallery-section-s2 section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <div class="wpo-gallery-container">
                            <div class="grid">
                                <div class="wpo-gallery-item">
                                    <a href="digital-analysis.html">
                                        <img src="assets/themes/consultar/assets/images/gallery/img-6.jpg" alt class="img img-responsive">
                                        <div class="wpo-gallery-text">
                                            <h3>Digital Analysis</h3>
                                            <i class="ti-plus"></i>
                                        </div>
                                    </a>
                                </div>
                            </div>
                                <div class="grid">
                                    <div class="wpo-gallery-item">
                                        <a href="corporate-finance.html">
                                            <img src="assets/themes/consultar/assets/images/gallery/img-7.jpg" alt class="img img-responsive">
                                            <div class="wpo-gallery-text">
                                                <h3>Corporate Finance</h3>
                                                <i class="ti-plus"></i>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                        <div class="grid">
                            <div class="wpo-gallery-item">
                                <a href="market-research.html">
                                    <img src="assets/themes/consultar/assets/images/gallery/img-8.jpg" alt class="img img-responsive">
                                    <div class="wpo-gallery-text">
                                        <h3>Market Research</h3>
                                        <i class="ti-plus"></i>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="grid">
                            <div class="wpo-gallery-item">
                                <a href="business-analysis-project.html">
                                    <img src="assets/themes/consultar/assets/images/gallery/img-9.jpg" alt class="img img-responsive">
                                    <div class="wpo-gallery-text">
                                        <h3>Business Analysis</h3>
                                        <i class="ti-plus"></i>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="grid">
                            <div class="wpo-gallery-item">
                                <a href="investment-strategy.html">
                                    <img src="assets/themes/consultar/assets/images/gallery/img-10.jpg" alt class="img img-responsive">
                                    <div class="wpo-gallery-text">
                                        <h3>Investment Strategy</h3>
                                        <i class="ti-plus"></i>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="grid">
                            <div class="wpo-gallery-item">
                                <a href="consumer-markets-project.html">
                                    <img src="assets/themes/consultar/assets/images/gallery/img-11.jpg" alt class="img img-responsive">
                                    <div class="wpo-gallery-text">
                                        <h3>Consumer Markets</h3>
                                        <i class="ti-plus"></i>
                                    </div>
                                </a>
                            </div>
                        </div> 
                        </div>
                    </div>
                </div> <!-- end row -->
            </div>
        </section>
        <!--End wpo-project-section-->
@endsection