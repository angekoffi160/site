@extends('layouts.ditech_master')
@section('content')

        <!-- start of breadcumb-section -->
        <div class="wpo-breadcumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-breadcumb-wrap">
                            <h2>Nous Contacter</h2>
                            <ul id="breadcrumb" itemprop="breadcrumb"><li><a href="index.html">Home</a></li>
                                <li><span>Contacts</span></li></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of wpo-breadcumb-section-->

        <!-- start wpo-contact-pg-section -->
        <section class="wpo-contact-pg-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-10 offset-lg-1">
                        <div class="office-info">
                            <div class="row">
                                <div class="col col-xl-4 col-lg-6 col-md-6 col-12">
                                    <div class="office-info-item">
                                        <div class="office-info-icon">
                                            <div class="icon">
                                                <img src="{{url('assets/themes/consultar/assets/images/icon/home.svg')}}" alt=>
                                            </div>
                                        </div>
                                        <div class="office-info-text">
                                            <h2>Adresse</h2>
                                            <p>Avenue lamblin , Immeuble macta</p>
                                        </div>
                                    </div>
                                </div>                                        
                                <div class="col col-xl-4 col-lg-6 col-md-6 col-12">
                                    <div class="office-info-item">
                                        <div class="office-info-icon">
                                            <div class="icon">
                                                <img src="{{url('assets/themes/consultar/assets/images/icon/mail-2.svg')}}" alt=>
                                            </div>
                                        </div>
                                        <div class="office-info-text">
                                            <h2>Mail</h2>
                                            <p>info@ditechnet.com <br>
                                                info@ditechnet.com
                                            </p>
                                        </div>
                                    </div>
                                </div>                                        
                                <div class="col col-xl-4 col-lg-6 col-md-6 col-12">
                                    <div class="office-info-item">
                                        <div class="office-info-icon">
                                            <div class="icon">
                                                <img src="{{url('assets/themes/consultar/assets/images/icon/app.svg')}}" alt=>
                                            </div>
                                        </div>
                                        <div class="office-info-text">
                                            <h2>Appel</h2>
                                            <p>+225 0556 555 429 <br>  +225 0556 519 023</p>
                                        </div>
                                    </div>
                                </div>                                        
                            </div>
                        </div>                      
                        <div class="wpo-contact-title">
                            <h2>Avez-vous une Question?</h2>
                           </div>
                        <div class="wpo-contact-form-area">




                            <form id="form_btn" class="contact-validation-active" method="post" action="https://consultar.webps.info/contacts.html">
                                <input type="hidden" name="nospam" value="" />
                                <div class="fullwidth">
                                    <input class="form-control" name="name" type="text" placeholder=" Nom*" value="">
                                </div>
                                <div>
                                    <input class="form-control" name="email" type="email" placeholder="Email*" value="">
                                </div>
                                <div>
                                    <input class="form-control" name="phone" type="text" placeholder="telephone*" value="">
                                </div>
                                <div class="fullwidth">
                                    <textarea class="form-control" name="message" placeholder="Message..."></textarea>
                                </div>
                                <div class="submit-area">
                                    <!--<button type="submit" name="contact" class="theme-btn">Get in Touch</button>-->
                                    <input type="submit" name="contact" id="submit-form-i" value="enoyer">
                                </div>
                            </form>                           
                        </div>
                    </div>                
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end wpo-contact-pg-section -->

        <!--  start wpo-contact-map -->
        <section class="wpo-contact-map-section">
            <h2 class="hidden">Contact map</h2>
            <div class="wpo-contact-map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d107058.184307901!2d-74.02306573957692!3d40.719886645158006!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sua!4v1643794867945!5m2!1sru!2sua" width="800" height="600" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </section>
        <!-- end wpo-contact-map -->
@endsection()