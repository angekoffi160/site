@extends('layouts.ditech_master')
@section('content')
        <!-- start of breadcumb-section -->
        <div class="wpo-breadcumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-breadcumb-wrap">
                            <h2>A propos de nous</h2>
                            <ul id="breadcrumb" itemprop="breadcrumb"><li><a href="{{route('acceuil')}}">Acceuil</a></li>
                                <li><span>A propos</span></li></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of wpo-breadcumb-section-->

        <!-- start of wpo-about-section -->
        <section class="wpo-about-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-12">
                        <div class="wpo-about-wrap">
                           <div class="wpo-about-img">
                              <img src="{{url('assets/themes/consultar/assets/images/cal.webp')}}" alt="About">
                              <div class="wpo-ab-shape-1"><img src="{{url('assets/themes/consultar/assets/images/ab-shape-1.png')}}" alt="About"></div>
                              <div class="wpo-ab-shape-2"><img src="{{url('assets/themes/consultar/assets/images/ab-shape-2.png')}}" alt="About"></div>
                           </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-12">
                        <div class="wpo-about-text">
        
                           <div class="wpo-about-icon-content">
                               <h2>Ditech<span> Consulting</span> group</h2>
                               <p><p> le groupe s’est donné
                                 comme objectif principale de de répondre aux besoins croissants 
                                 des entreprises concernant l’intégration de solutions informatiques.
                                  Elle essaye  également de mettre à leur disposition 
                                   des services adaptés à leur transformation digitale. Ainsi,
                                    pour atteindre ses objectifs , l’entreprise attache de
                                     l’importance à certaines valeur telle que : l’esprit d’équipe,
                                      le dynamisme et l’efficacité </p></p>
                               <div class="signeture">
                                   <span><img src="{{url('assets/themes/consultar/assets/images/signeture.png')}}" alt="About"></span>
                                   
                               </div>
                               <a class="theme-btn" href="#">DIDIER GBAI, CEO</a>
                           </div>
                        </div>
                    </div>
                </div>
            </div>      
        </section>
        <!-- end of wpo-about-section -->

        <!-- start of wpo-service-section -->
        <section class="wpo-service-section section-padding" >
            <div class="container"style="positon: relative; top:-65vh">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-section-title">
                            <span> Services</span>
                            <h2>Explorez nos Services</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                                            <div class="col-lg-4 col-md-6 col-12">
                            <div class="wpo-service-item">
                                <div class="wpo-blog-img">
                                    <img src="{{url('assets/themes/consultar/assets/images/dev.webp')}}" width="100%" height="90%" alt="Post Four">
                                </div>
                                <div class="wpo-service-text">
                                    <h2 style="text-align: center"><a href="corporate-finance-s.html">DEVELOPPEMENT</a></h2>
                                   
                                    <div class="btns" style="position:relative; left:10vh;">
                                        <a href="services.html" class="btn theme-btn">Plus</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="wpo-service-item">
                                <div class="wpo-blog-img">
                                    <img src="{{url('assets/themes/consultar/assets/images/audit.jpg')}}" width="100%" height="90%" alt="Post Four">
                                </div>
                                <div class="wpo-service-text">
                                    <h2 style="text-align: center"><a href="market-research-s.html">AUDIT SI</a></h2>
                                    <div class="btns" style="position:relative; left:10vh;">
                                        <a href="services.html" class="btn theme-btn">Plus</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="wpo-service-item">
                                <div class="wpo-blog-img">
                                    <img src="{{url('assets/themes/consultar/assets/images/form1.jpg')}}" width="100%" height="90%" alt="Post Four">
                                </div>
                                <div class="wpo-service-text">
                                    <h2 style="text-align: center"><a href="business-analysis.html">FORMATION</a></h2>
                                    
                                    <div class="btns" style="position:relative; left:10vh;">
                                        <a href="services.html" class="btn theme-btn">Plus</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                       
                         
                </div>
            </div>
        </section>        
        <!-- end wpo-fun-fact-section -->


      
    @endsection()
   