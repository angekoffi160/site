@extends('layouts.ditech_master')

@section('content')

<section class="wpo-hero-section-1 wpo-hero-section-2">
    <div class="container"  >
        <div class="row">
            <div class="col col-xs-6 col-lg-6">
                <div class="wpo-hero-section-text" style="margin-top: -55vh;">
                    <div class="wpo-hero-title">
                        <h2>Intégrateur de solutions informatiques</h2>
                    </div>
                    <div class="wpo-hero-subtitle">
                        <p>Analyse-Etude-Conception-Réalisation</p>
                    </div>
                    <div class="btns">
                        <a href="#" class="btn theme-btn">A propos</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="right-vec"  >
        <div class="right-img" style="margin-top: -27vh;">
            <div class="r-img" >
                <img src="{{url('assets/themes/consultar/assets/images/Technology.webp')}}" alt="">
                <!-- <div class="svg-shape">
                    <svg width="826" height="309" viewBox="0 0 826 309" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M100.983 35.4286C112.365 13.6512 134.903 0 159.476 0H754.544C785.255 0 811.908 21.1833 818.841 51.102L823.663 71.9141C825.212 78.5972 825.709 85.4808 825.136 92.317L812.063 248.505C809.201 282.702 780.61 309 746.293 309H66.9661C17.3837 309 -14.4933 256.371 8.4734 212.429L100.983 35.4286Z" fill="#edf2f8"/>
                     </svg>
                </div> -->
                <div class="pop-up-video">
                    <div class="video-holder">
                        <a href="{{url('https://www.youtube.com/embed/7Jv48RQ_2gk')}}" class="video-btn" data-type="iframe"><i class="fi flaticon-play"></i></a>
                    </div>
                </div>
            </div>
            <div class="wpo-happy-client">
                  <div class="wpo-happy-client-text">
                      <h3>50+</h3>
                      <p>Clients satisfaits</p>
                  </div>
                  <div class="wpo-happy-client-img">
                      <ul class="wpo-happy-client-slide owl-carousel">
                          <li><img src="{{url('assets/themes/consultar/assets/images/noir2.webp')}}" alt=""></li>
                          <li><img src="{{url('assets/themes/consultar/assets/images/noir2.webp')}}" alt=""></li>
                          <li><img src="{{url('assets/themes/consultar/assets/images/noir2.webp')}}" alt=""></li>
                          <li><img src="{{url('assets/themes/consultar/assets/images/noir2.webp')}}" alt=""></li>
                      </ul>
                  </div>
            </div>   
        </div>
    </div>
</section>
<!-- end of wpo-hero-section-1 slider -->
<!-- start of wpo-service-section -->
<section class="wpo-service-section section-padding" >
    <div class="container"style="positon: relative; top:-65vh">
        <div class="row">
            <div class="col-12">
                <div class="wpo-section-title">
                    <span> Services</span>
                    <h2>Explorez nos Services</h2>
                </div>
            </div>
        </div>
        <div class="row">
                                    <div class="col-lg-4 col-md-6 col-12">
                    <div class="wpo-service-item">
                        <div class="wpo-blog-img">
                            <img src="{{url('assets/themes/consultar/assets/images/dev.webp')}}" width="100%" height="90%" alt="Post Four">
                        </div>
                        <div class="wpo-service-text">
                            <h2 style="text-align: center"><a href="corporate-finance-s.html">DEVELOPPEMENT</a></h2>
                           
                            <div class="btns" style="position:relative; left:10vh;">
                                <a href="services.html" class="btn theme-btn">Plus</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="wpo-service-item">
                        <div class="wpo-blog-img">
                            <img src="{{url('assets/themes/consultar/assets/images/audit.jpg')}}" width="100%" height="90%" alt="Post Four">
                        </div>
                        <div class="wpo-service-text">
                            <h2 style="text-align: center"><a href="market-research-s.html">AUDIT SI</a></h2>
                            <div class="btns" style="position:relative; left:10vh;">
                                <a href="services.html" class="btn theme-btn">Plus</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="wpo-service-item">
                        <div class="wpo-blog-img">
                            <img src="{{url('assets/themes/consultar/assets/images/form1.jpg')}}" width="100%" height="90%" alt="Post Four">
                        </div>
                        <div class="wpo-service-text">
                            <h2 style="text-align: center"><a href="business-analysis.html">FORMATION</a></h2>
                            
                            <div class="btns" style="position:relative; left:10vh;">
                                <a href="services.html" class="btn theme-btn">Plus</a>
                            </div>
                        </div>
                    </div>
                </div>
                
               
                 
        </div>
    </div>
</section>
<!-- end of wpo-service-section -->


<!-- start of wpo-about-section -->
<section class="wpo-about-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-12">
                <div class="wpo-about-wrap">
                   <div class="wpo-about-img">
                      <img src="{{url('assets/themes/consultar/assets/images/cal.webp')}}" alt="About">
                      <div class="wpo-ab-shape-1"><img src="{{url('assets/themes/consultar/assets/images/ab-shape-1.png')}}" alt="About"></div>
                      <div class="wpo-ab-shape-2"><img src="{{url('assets/themes/consultar/assets/images/ab-shape-2.png')}}" alt="About"></div>
                   </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-12">
                <div class="wpo-about-text">

                   <div class="wpo-about-icon-content">
                       <h2>Ditech<span> Consulting</span> group</h2>
                       <p><p> le groupe s’est donné
                         comme objectif principale de de répondre aux besoins croissants 
                         des entreprises concernant l’intégration de solutions informatiques.
                          Elle essaye  également de mettre à leur disposition 
                           des services adaptés à leur transformation digitale. Ainsi,
                            pour atteindre ses objectifs , l’entreprise attache de
                             l’importance à certaines valeur telle que : l’esprit d’équipe,
                              le dynamisme et l’efficacité </p></p>
                       <div class="signeture">
                           <span><img src="{{url('assets/themes/consultar/assets/images/signeture.png')}}" alt="About"></span>
                           
                       </div>
                       <a class="theme-btn" href="#">DIDIER GBAI, CEO</a>
                   </div>
                </div>
            </div>
        </div>
    </div>      
</section>
<!-- end of wpo-about-section -->



<!--Start wpo-gallery-sect  ion-->  
<section class="wpo-gallery-section section-padding">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-sm-8">
                <div class="wpo-section-title">
                
                    <h2>Projets réalisés</h2>
                </div>
            </div>
            <div class="col-lg-6 col-sm-4">
                <div class="wpo-section-title-button">
                    <a href="projects.html">More Projects</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col col-xs-12 sortable-gallery">
                <div class="wpo-gallery-container">
                    <div class="grid">
                    <div class="wpo-gallery-item">
                        <a href="digital-analysis.html">
                            <img src="{{url('assets/themes/consultar/assets/images/semi.jpg')}}" alt class="img img-responsive">
                            <div class="wpo-gallery-text">
                                <h3>SYGED</h3>
                                <i class="ti-plus"></i>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="grid">
                    <div class="wpo-gallery-item">
                        <a href="corporate-finance.html">
                            <img src="{{url('assets/themes/consultar/assets/images/semi.jpg')}}" alt class="img img-responsive">
                            <div class="wpo-gallery-text">
                                <h3>SYGED</h3>
                                <i class="ti-plus"></i>
                            </div>
                        </a>
                    </div>
                </div>
                    <div class="grid">
                        <div class="wpo-gallery-item">
                            <a href="market-research.html">
                                <img src="{{url('assets/themes/consultar/assets/images/semi.jpg')}}" alt class="img img-responsive">
                                <div class="wpo-gallery-text">
                                    <h3>SYGED</h3>
                                    <i class="ti-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="grid">
                        <div class="wpo-gallery-item">
                            <a href="business-analysis-project.html">
                                <img src="{{url('assets/themes/consultar/assets/images/semi.jpg')}}" alt class="img img-responsive">
                                <div class="wpo-gallery-text">
                                    <h3>SYGED</h3>
                                    <i class="ti-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div> 
                </div>
            </div>
        </div> <!-- end row -->
    </div>
</section>
<!--End wpo-gallery-section-->
<!--Start wpo-testimonial-section-->  
<section class="wpo-testimonial-section section-padding">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <div class="wpo-testimonial-title">
                    <h2><i class="fi flaticon-left-quote"></i> Ce que <span>Nos Clients</span> pensent</h2>
                </div>
            </div>
        </div>
        <div class="wpo-testimonial-items wpo-testimonial-slider owl-carousel">
            <div class="wpo-testimonial-item">
                <div class="wpo-testimonial-avatar">
                   <img src="{{url('assets/themes/consultar/assets/images/noir2.webp')}}" alt="Home">
                </div>
                <div class="wpo-testimonial-text">
                    <p>j'ai été très satisfait du service offert par l'entreprise.
                         elle m'a aidé à mettre en place unsystème de gestion de restaurant</p>
                    <div class="wpo-testimonial-text-btm">
                        <h3>Jenny Watson</h3>
                        <span>DGPE</span>
                    </div>
                </div>
            </div>                    <div class="wpo-testimonial-item">
                <div class="wpo-testimonial-avatar">
                   <img src="{{url('assets/themes/consultar/assets/images/noir2.webp')}}" style="width:60px; heigth:40px" alt="Home">
                </div>
                <div class="wpo-testimonial-text">
                    <p>j'ai été très satisfait du service offert par l'entreprise.
                        elle m'a aidé à mettre en place unsystème de gestion de restaurant</p>
                    <div class="wpo-testimonial-text-btm">
                        <h3>Harry Abraham</h3>
                        <span>AZALAI</span>
                    </div>
                </div>
            </div>                    <div class="wpo-testimonial-item">
                <div class="wpo-testimonial-avatar">
                   <img src="{{url('assets/themes/consultar/assets/images/noir.webp')}}" alt="Home">
                </div>
                <div class="wpo-testimonial-text">
                    <p>j'ai été très satisfait du service offert par l'entreprise.
                        elle m'a aidé à mettre en place un système de gestion de restaurant</p>
                    <div class="wpo-testimonial-text-btm">
                        <h3>Ron Di-soza</h3>
                        <span>DGPE</span>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</section>
<!--End wpo-testimonial-section-->
<!--Start wpo-support-section-->  
<section class="wpo-support-section mb-4">
    <div class="container">
        <div class="wpo-support-wrapper">
            <div class="wpo-support-text">
                <h2>Avez vous une préocupation ?</h2>
            </div>
            <div class="wpo-support-btn">
                <a href="{{route('contact')}}">CONTACT</a>
            </div>
        </div>
    </div>
</section>  
<!--End wpo-support-section-->



@endsection()