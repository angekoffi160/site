<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from consultar.webps.info/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Jul 2022 16:33:56 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="" />
	<meta name="description" content="We bring the right people together.">
	<meta name="author" content="Admin">    
    <link rel="shortcut icon" type="image/png" href="assets/themes/consultar/assets/images/favicon.png">
    <title>Home | Consultar - Consulting Business MODX Theme</title>
    <link href="{{ url('assets/themes/consultar/assets/css/themify-icons.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/flaticon.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/animate.css')}}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/owl.theme.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/slick.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/slick-theme.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/swiper.min.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/owl.transitions.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/jquery.fancybox.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/odometer-theme-default.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/component.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ url('assets/themes/consultar/assets/css/responsive.css') }}" rel="stylesheet">
</head>


<body>
    <!-- start page-wrapper -->
    <div class="page-wrapper">
        <!-- start preloader -->
        <div class="preloader">
            <div class="angular-shape">
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div class="spinner">
              <div class="double-bounce1"></div>
              <div class="double-bounce2"></div>
            </div>
        </div>
        <!-- end preloader -->
        <!-- Start header -->
        <header id="header" class="wpo-site-header">
            <nav class="navigation navbar navbar-expand-lg navbar-light">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-lg-3 col-md-3 col-6">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggler open-btn">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar first-angle"></span>
                                    <span class="icon-bar middle-angle"></span>
                                    <span class="icon-bar last-angle"></span>
                                </button>
                                <a class="navbar-brand"><img src="{{ url('assets/themes/consultar/assets/images/logo-ditech.png') }}" style="position: relative; left:10vh;"  width="120vh" height="120vh" alt=""></a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-2 col-1">
                            <div id="navbar" class="collapse navbar-collapse navigation-holder">
                                <button class="close-navbar"><i class="ti-close"></i></button>
                                <ul class="nav navbar-nav mb-2 mb-lg-0">
                                    <li class="active">
    									<a class="active" href="{{route('acceuil')}}">
    										Acceuil
    									</a>
    								</li>
                                    <li>
        								<a href="{{route('propos')}}">
        									A propos
        								</a>
        							</li>
                                    <li>
        								<a href="{{route('services')}}">
        									Services
        								</a>
        							</li>
                                    <li>
        								<a href="{{route('projet')}}">
        									Projets
        								</a>
        							</li>
                                        <li class="menu-item-has-children">
    									<a href="#">
    											Pages
    										</a>
    										<ul class="sub-menu">
                                    <li>
        								<a href="{{route('pricing')}}">
        									Pricing
        								</a>
        							</li>
                                    <li>
        								<a href="{{route('testimonial')}}">
        									Testimonial
        								</a>
        							</li>

                                            </ul>
    									</li>
											
                                    <li>
        								<a href="{{route('blog')}}">
        									Blog
        								</a>
        							</li>
                                    <li>
        								<a href="{{route('contact')}}">
        									Contacts
        								</a>
        							</li>

                                </ul>
                                <div id="dl-menu" class="dl-menuwrapper">
                                    <button class="dl-trigger">Open Menu</button>
                                    <ul class="dl-menu">
                                    <li class="active">
    									<a class="active" href="{{route('acceuil')}}">
    										Acceuil
    									</a>
    								</li>
                                    <li>
        								<a href="{{route('propos')}}">
        									A props
        								</a>
        							</li>
                                    <li>
        								<a href="{{route('services')}}">
        									Services
        								</a>
        							</li>
                                    <li>
        								<a href="{{route('projet')}}">
        									Projects
        								</a>
        							</li>
                                        <li>
    									<a href="#">
    											Pages
    										</a>
    										<ul class="dl-submenu">
                                    <li>
        								<a href="{{route('pricing')}}">
        									Pricing
        								</a>
        							</li>
                                    <li>
        								<a href="{{route('testimonial')}}">
        									Testimonial
        								</a>
        							</li>

                                            </ul>
    									</li>
											
                                    <li>
        								<a href="{{route('blog')}}">
        									Blog
        								</a>
        							</li>
                                    <li>
        								<a href="{{route('contact')}}">
        									Contacts
        								</a>
        							</li>

                                    </ul>
                                </div><!-- /dl-menuwrapper -->
                            </div><!-- end of nav-collapse -->
                        </div>
                        <div class="col-lg-3 col-md-4 col-4">
                            <div class="header-right">
                                <div class="close-form">
                                    <a target="_blank" class="theme-btn" href="#">Ditech</a>
                                </div>
                                <div class="header-search-form-wrapper">
                                    <div class="cart-search-contact">
                                        <button class="search-toggle-btn"><i class="fi flaticon-magnifiying-glass"></i></button>
                                        <div class="header-search-form">
                                            <form role="search" action="{{ url('https://consultar.webps.info/search.html') }}" method="get" id="searchform"  class="form">
                                                <div>
                                                    <input type="text" class="form-control" name="search" placeholder="Search here...">
                                                    <button  type="submit" class="btn"><i class="fi flaticon-magnifiying-glass"></i></button>
                                                    <input type="hidden" name="id" value="16" />
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end of container -->
            </nav>
        </header>
        <!-- end of header -->
        










        @yield('content')










        <!-- start of wpo-site-footer-section -->
        <footer class="wpo-site-footer">
            <div class="wpo-upper-footer">
                <div class="container">
                    <div class="row">
                        <div class="col col-lg-3 col-md-6 col-sm-12 col-12">
                            <div class="widget about-widget">
                                <div class="logo widget-title">
                                    <img src="{{ url('assets/themes/consultar/assets/images/logo-new.jpg') }}" width="120vh" height="120vh" alt="Consultar - Consulting Business MODX Theme">
                                </div>
                                <p>Nous mettons tout en oeuvre pour vous fournir un service de qualité et vous accompagner dans l'intégration de solutions informatiques</p>        
                                <ul>
                                    <li><a target="_blank" href="{{ url('https://facebook.com/') }}"><i class="ti-facebook"></i></a></li>
                                    <li><a target="_blank" href="{{ url('https://twitter.com/') }}"><i class="ti-twitter-alt"></i></a></li>
                                    <li><a target="_blank" href="{{ url('https://www.instagram.com/') }}"><i class="ti-instagram"></i></a></li>
                                    <li><a target="_blank" href="{{ url('https://www.google.com/') }}"><i class="ti-google"></i></a></li> 
                                </ul>
                            </div>
                        </div>
                        <div class="col col-lg-3 col-md-6 col-sm-12 col-12">
                            <div class="widget wpo-service-link-widget">
                                <div class="widget-title">
                                    <h3>Contact </h3>
                                </div>
                                <div class="contact-ft">
                                    <ul>                                        
                                <li><i class="fi flaticon-location"></i>Avenue lamblin , Immeuble macta</li>                                        
                                <li><i class="fi flaticon-send"></i>info@ditechnet.com</li>                                        
                                <li><i class="fi flaticon-phone-call"></i>+225 0556 555 429 <br>  +225 0556 519 023</li>
                                    </ul>   
                                </div>
                            </div>
                        </div>
                        <div class="col col-lg-3 col-md-6 col-sm-12 col-12">
                            <div class="widget link-widget">
                                <div class="widget-title">
                                    <h3>Services </h3>
                                </div>
                                <ul>
                                    <li><a href="#">Developpement</a></li>
                                    <li><a href="#">Audit et sécurité</a></li>
                                    <li><a href="#">Formation et certification</a></li>
                                     
                                </ul>
                            </div>
                        </div>
                        
                        <div class="col col-lg-3 col-md-6 col-sm-12 col-12">
                            <div class="widget instagram">
                                <div class="widget-title">
                                    <h3>Projects</h3>
                                </div>
                                <ul class="d-flex">
                                    <li><a href="digital-analysis.html"><img src="{{ url('assets/themes/consultar/assets/images/dev.webp')}}"></a></li>    
                                    <li><a href="corporate-finance.html"><img src="{{ url('assets/themes/consultar/assets/images/audit.jpg') }}" alt="Corporate Finance"></a></li>    
                                    <li><a href="market-research.html"><img src="{{ url('assets/themes/consultar/assets/images/form1.jpg') }}" alt="Market Research"></a></li>    
                                    <li><a href="business-analysis-project.html"><img src="{{ url('assets/themes/consultar/assets/images/form1.jpg') }}" alt="Business Analysis"></a></li>    
                                    <li><a href="investment-strategy.html"><img src="{{ url('assets/themes/consultar/assets/images/audit.jpg') }}" alt="Investment Strategy"></a></li>    
                                    <li><a href="consumer-markets-project.html"><img src="{{ url('assets/themes/consultar/assets/images/dev.webp') }}" alt="Consumer Markets"></a></li>     
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> <!-- end container -->
            </div>
            <div class="wpo-lower-footer">
                <div class="container">
                    <div class="row">
                        <div class="col col-xs-12">
                            <p class="copyright"> Copyright &copy; 2022 ditech All Rights Reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end of wpo-site-footer-section -->
    </div>
    <!-- end of page-wrapper -->
    <!-- All JavaScript files
    ================================================== -->
    <script src="{{url('assets/themes/consultar/assets/js/jquery.min.js') }}"></script>
    <script src="{{url('assets/themes/consultar/assets/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Plugins for this template -->
    <script src="{{url('assets/themes/consultar/assets/js/modernizr.custom.js') }}"></script>
    <script src="{{url('assets/themes/consultar/assets/js/jquery.dlmenu.js') }}"></script>
    <script src="{{url('assets/themes/consultar/assets/js/jquery-plugin-collection.js') }}"></script>
    <!-- Custom script for this template -->
    <script src="{{url('assets/themes/consultar/assets/js/script.js') }}"></script>
    <!-- Share buttons -->
    <script type="text/javascript" src="{{ url('s7.addthis.com/js/300/addthis_widget.js#pubid=ra-62013f10644665a7') }}"></script>
</body>


<!-- Mirrored from consultar.webps.info/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Jul 2022 16:34:05 GMT -->
</html>